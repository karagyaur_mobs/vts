<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'vts');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'msstudio.mysql.tools');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'n>X^p!jHPxqDf?W89?d5--*,1MNIlYpCn#+#d[!`o!U>70Z~eXdz`4@QOq.LN>w2');
define('SECURE_AUTH_KEY',  '#OT!`[[4N;Fg5p{ ,z;f(6XWX)M#1]jdYcX+]#|sWarf^K9ka;H]h+b3ut,J[vVR');
define('LOGGED_IN_KEY',    '-YSa1}~;nh:By[N7~6`%+/0@TE2bNGf29-&rfAE~2gS@jiqZ(uV;Z*d8*x,Yt;ny');
define('NONCE_KEY',        '*T{%I?.~U4D:)G:XR;W9.5Y*6>3{`Yz-559YgJsZ0dUd[4oC72NhkL]SPKz3O!++');
define('AUTH_SALT',        'N64q_{}dND~>A$.Hsh)eUEAIH+U$-U8IjFHvQiGSgp}i/U{;TJ8^c]D1+M<;Wf91');
define('SECURE_AUTH_SALT', 'k/H0!(m[J9.l]mqX}>mAW%Bj&vzaq>/jJ<&wll#pv0eP- !2|#} se_@w:}?^C!=');
define('LOGGED_IN_SALT',   'q0i$DR*y42.@q-3OM5?+,pd>(bm|tN~z;0,e3)voJ>NKA.1!!dQ@99E* MYYK]0!');
define('NONCE_SALT',       'hz@T5h22kH9WJ&[-KBt*VCk]ICsg`5y%p[YawqEI} Ad#5jj EUS%V7q?BPi;s1Z');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
