var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var plumber = require('gulp-plumber');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var minjs = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');


gulp.task('online', ['sass', 'html','jsmin'], function () {

    browserSync.init({
        port:8080,
        open:true,
        notify:false,
        // tunnel:true,
        proxy:'vts/'
    });

    gulp.watch('wp-content/themes/custom/scss/*.scss', ['sass']);
    gulp.watch('wp-content/themes/custom/*.php', ['html']);
    gulp.watch('wp-content/themes/custom/src/*.js', ['jsmin']);
});

gulp.task('sass',function () {
    gulp.src('wp-content/themes/custom/scss/*.scss')
        .pipe(plumber())
        .pipe(sass({outputStyle: 'compact'}))
        .pipe(sass({errLogToConsole:true}))
        .pipe(autoprefixer({
            browsers:['last 10 versions'],
            cascade:false
        }))
        // .pipe(cssmin())
        .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest('wp-content/themes/custom/css/'))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('jsmin', function () {
    gulp.src('wp-content/themes/custom/scr/*.js')
        .pipe(minjs())
        .pipe(rename({extname: '.min.js'}))
        .pipe(gulp.dest('wp-content/themes/custom/js/'))
});


gulp.task('html',function () {
    return gulp.src('wp-content/themes/custom/*.php')
        .pipe(browserSync.reload({stream: true}));
});


gulp.task('default',['online']);