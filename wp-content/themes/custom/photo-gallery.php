<?php
/**
 * Created by PhpStorm.
 * User: mobios starts
 * Date: 16.10.2018
 * Time: 10:05
 * Template Name: Фото
 */
get_header(); ?>

<section class="container">
	<?php dimox_breadcrumbs() ?>
</section>

<section class="gallery">
	<div class="container">
		<h3 class="section__title"><?php echo get_the_title() ?></h3>
		<div class="gallery-page__inner">

				<?php $images = acf_photo_gallery('gallery-page', $post->ID); ?>
				<?php if( $images ):
					foreach($images as $image):
						$full_image_url = $image['full_image_url']; //Full size image url
						$image_url = acf_photo_gallery_resize_image($full_image_url, 274, 183); //Resized size to image url?>
							<div class="about__slide gallery-page__slide">
								<a class="fancybox" data-fancybox="gallery" href="<?php echo $full_image_url; ?>">
									<img src="<?php echo $image_url; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" class="about__slide-img">
								</a>
							</div>
					<?php endforeach;
				endif; ?>
		</div>
	</div>
</section>


<?php get_footer(); ?>
