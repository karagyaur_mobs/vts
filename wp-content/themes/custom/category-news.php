<?php
/**
 * Created by PhpStorm.
 * User: mobios starts
 * Date: 11.10.2018
 * Time: 12:09
 */
get_header() ;?>

	<section class="breadcrumbs-wrap container">
		<?php dimox_breadcrumbs() ?>
	</section>

	<section class="trading">
		<div class="container">
			<h3 class="section__title"><?php echo single_cat_title() ?></h3>
            <div class="news__inner row">
<!--				--><?php
//				global $post;
//				$args = array('posts_per_page' => -1, 'category' => 4);
//				$myposts = get_posts($args);
//				foreach ($myposts as $post) {
//					setup_postdata($post);
//					?>
<!--                    <div class="news__item col-md-4">-->
<!--                        <a class="news__poster" href="--><?php //the_permalink(); ?><!--">--><?php //the_post_thumbnail( array(350, 235) ); ?><!--</a>-->
<!---->
<!--                        <a class="news__head" href="--><?php //the_permalink(); ?><!--">-->
<!--							--><?php //echo mb_strimwidth(get_the_title(), 0, 55, '...');?>
<!--                        </a>-->
<!--                        <p class="news__descr">--><?php //trimmed_content(300 ); ?><!--</p>-->
<!--                        <div class="news__read-more-wrap">-->
<!--                            <span class="">--><?php //echo get_the_date('d.m.Y')?><!--</span>-->
<!--                            <a class="news__read-more" href="--><?php //the_permalink(); ?><!--">Читати далi...</a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--					--><?php
//				}
//				wp_reset_postdata();
//				?>


                <?php

                $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args = array(
	                'posts_per_page' => 9,
	                'cat' => 4,
	                'paged' => $current_page // текущая страница
                );

                $query = new WP_Query( $args );

                // Цикл
                if ( $query->have_posts() ) {
	                while ( $query->have_posts() ) {
		                $query->the_post(); ?>
		                <div class="news__item col-12 col-md-6 col-lg-3">
                        <a class="news__poster" href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(350, 235) ); ?></a>

                        <a class="news__head" href="<?php the_permalink(); ?>">
							<?php echo mb_strimwidth(get_the_title(), 0, 55, '...');?>
                        </a>
                        <p class="news__descr"><?php trimmed_content(300 ); ?></p>
                        <div class="news__read-more-wrap">
                            <span class=""><?php echo get_the_date('d.m.Y')?></span>
                            <a class="news__read-more" href="<?php the_permalink(); ?>">Читати далi...</a>
                        </div>
                    </div>
	               <?php }
                } else {
	                // Постов не найдено
                }
                /* Возвращаем оригинальные данные поста. Сбрасываем $post. */
                the_posts_pagination();
                wp_reset_postdata();

                ?>


            </div>
        </div>
	</section>

<?php get_footer() ;?>