<?php
/**
 * Created by PhpStorm.
 * User: mobios starts
 * Date: 10.10.2018
 * Time: 17:33
 * Template Name: Об’єкти торгівлі
 */
get_header() ;?>

	<section class="breadcrumbs-wrap container">
		<?php dimox_breadcrumbs() ?>
	</section>

	<section class="shops">
		<div class="container">
			<h3 class="section__title"><?php echo get_the_title() ?></h3>
			<div class="shops__inner row">
                <?php for ($i = 1; $i <= 10; $i++) { ?>
	                <?php if (get_field('city-' . $i)) { ?>
                        <div class="col-xs-12 col-sm-6 col-lg-3 shops__item">
			                <?php the_field('map-code-' . $i) ?>
                            <p class="shops__city"><?php the_field('city-' . $i) ?></p>
                            <p class="shops__address"><?php the_field('address-' . $i) ?></p>
                            <a class="shops__phone" href="tel:<?php the_field('phone-' . $i) ?>"><?php the_field('phone-' . $i) ?></a>
                        </div>
	                <?php } ?>
                <?php } ?>
			</div>
		</div>
	</section>

<?php get_footer() ;?>