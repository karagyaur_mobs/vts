<?php  
/*
	The template for displaying pages
*/
?>

<?php get_header(); ?>

<div class="page-content">
	
	<!--breadcrumbs section-->
	<section class="breadcrumbs-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<ul class="breadcrumbs">
						<li class="breadcrumbs__item"><a href="/" class="breadcrumbs__link home-link">Головна</a></li>
						<li class="breadcrumbs__item"><?php the_title() ;?></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--breadcrumbs section-END-->
	
	<section class="page-content__inner">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="category-title blue-drib">
						<h2 class="category-title__content"><?php the_title(); ?></h2>
					</div>
				</div>
			</div>
			<div class="row text-editor-content">
				<div class="col-12">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</section>

</div>

<?php get_footer(); ?>