<?php
/*
Template Name: Search
*/
?>

<?php get_header();?>

<section class="container">
	<?php dimox_breadcrumbs() ?>
</section>

<!--custom-search section-->
<section class="custom-search">
    <div class="container">
        <div class="news__inner row">
	        <?php

	        // задаем нужные нам критерии выборки данных из БД
	        $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
	        $args = array(
		        'posts_per_page' => 9,
		        'cat' => 4,
		        'paged'           => $current_page // текущая страница
	        );

	        $query = new WP_Query( $args );

	        // Цикл
	        if ( $query->have_posts() ) {
		        while ( $query->have_posts() ) {
			        $query->the_post(); ?>
                    <div class="news__item col-md-4">
                        <a class="news__poster" href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(350, 235) ); ?></a>

                        <a class="news__head" href="<?php the_permalink(); ?>">
					        <?php echo mb_strimwidth(get_the_title(), 0, 55, '...');?>
                        </a>
                        <p class="news__descr"><?php trimmed_content(300 ); ?></p>
                        <div class="news__read-more-wrap">
                            <span class=""><?php echo get_the_date('d.m.Y')?></span>
                            <a class="news__read-more" href="<?php the_permalink(); ?>">Читати далi...</a>
                        </div>
                    </div>
		        <?php }
	        } else {
		        // Постов не найдено
	        }
	        /* Возвращаем оригинальные данные поста. Сбрасываем $post. */
	        the_posts_pagination();
	        wp_reset_postdata();

	        ?>
        </div>
    </div>
</section>
    <!--custom-search section -END-->


<?php get_footer(); ?>
