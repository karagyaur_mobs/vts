<?php
/**
 * Created by PhpStorm.
 * User: mobios starts
 * Date: 12.07.2018
 * Time: 11:29
 * Template Name: Контакти
 */
get_header(); ?>

	<section class="breadcrumbs-wrap container">
		<?php dimox_breadcrumbs() ?>
	</section>

    <section class="contacts">
        <div class="container">
            <h3 class="section__title">Контакти</h3>
            <div class="contacts__inner row">
                <div class="col-md-5 contacts__info">
                    <h3 class="contacts__title">Контактна інформація</h3>
                    <div class="contacts__info-inner">
                        <div class="contacts__info-item">
                            <address class="contacts__info-txt">
                                м. Київ
                                Молодогвардійська, 28а
                            </address>
                        </div>
                        <div class="contacts__info-item">
                            <a class="contacts__info-txt contacts__info-txt_link" href="tel:+ 38 (063) 432-34-56">+38 (063) 432-34-56</a>
                            <a class="contacts__info-txt contacts__info-txt_link" href="tel:+ 38 (063) 432-34-56">+38 (063) 432-34-56</a>
                        </div>
                        <div class="contacts__info-item">
                            <a class="contacts__info-txt contacts__info-txt_link" href="mailto:info@gmail.com">info@gmail.com</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 offset-md-1">
                    <h3 class="contacts__title">Зв’язатись з нами</h3>
                    <form class="callback">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                    <label class="contacts__label" for="name">Ваше ім'я</label>
                                    <input class="contacts__input" type="text" id="name" name="Iм'я" placeholder="Введіть Ваше ім'я">
                                    <label class="contacts__label" for="mail">Ваш Email</label>
                                    <input class="contacts__input" type="email" id="mail" name="Пошта" placeholder="Введіть Ваш e-mail">
                                    <label class="contacts__label" for="phone">Ваш номер телефону</label>
                                    <input class="contacts__input" type="tel" id="phone" name="Телефон" placeholder="Введіть Ваш номер телефону">
                                </div>
                            <div class="col-12 col-md-6">
                                    <label class="contacts__label" for="message">Текст повідомлення</label>
                                    <textarea class="contacts__input contacts__input_txtarea" name="Повідомлення" id="message" placeholder="Введіть текст повідомлення"></textarea>
                                </div>
                            <button class="green-btn" type="submit">
                                    Надіслати
                                </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="contacts__map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2542.0734045822055!2d30.447927315730308!3d50.42110397947133!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4ceb3aef9bffb%3A0x145b3ee6ee0e343a!2zMjhBLCDRg9C7LiDQnNC-0LvQvtC00L7Qs9Cy0LDRgNC00LXQudGB0LrQsNGPLCAyONCQLCDQmtC40LXQsiwgMDIwMDA!5e0!3m2!1sru!2sua!4v1539334189438" width="100%" height="332" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </section>






<?php get_footer(); ?>