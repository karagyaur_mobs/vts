<?php
/**
 * Created by PhpStorm.
 * User: mobios starts
 * Date: 10.10.2018
 * Time: 14:53
 * Template Name: Торгівля
 */
get_header() ;?>

<section class="breadcrumbs-wrap container">
	<?php dimox_breadcrumbs() ?>
</section>

<section class="trading">
	<div class="container">
		<h3 class="section__title">Продукція</h3>
		<div class="trading__inner">
			<div class="trading__item">
				<div class="trading__examples">
					<div class="trading__poster trading__poster1">
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1.png" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1-1.png" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1-2.png" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1-3.png" alt="">
						</div>
					</div>
					<div class="trading__nav trading__nav1">
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1.png" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1-1.png" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1-2.png" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1-3.png" alt="">
						</div>
					</div>
				</div>
				<div class="trading__description">
					<h2 class="trading__title">Сувенірна продукція</h2>
					<p class="trading__txt">
						Сувенірні чашки із логотипом компанії, або корпоративною символікою фірми дозволяють
						ефектно та яскраво виділитися на загальному конкурентному тлі. Сьогодні багато компаній
						успішно замовляють виготовлення сувенірних чашок у фірмовому стилі для проведення
						різних акцій, або тематичних заходів. Використовують подібні чашки для роздачі, в
						якості призів, або заохочень. Така продукція має всі шанси для подальшого перетікання
						у взаємовигідну та ефективну співпрацю, приємно дивуючи та піднімаючи до себе інтерес
						у клієнтської аудиторії.
					</p>
					<p class="trading__addresses-txt">Адреси за якими ви можете придбати товар:</p>
					<a class="trading__addresses-btn" href="/obyekti-torgivli">Об’єкти торгівлі</a>
				</div>
			</div>
			<div class="trading__item">
				<div class="trading__examples">
					<div class="trading__poster trading__poster2">
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-2.png" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1-1.png" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1-2.png" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1-3.png" alt="">
						</div>
					</div>
					<div class="trading__nav trading__nav2">
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-2.png" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1-1.png" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1-2.png" alt="">
						</div>
						<div>
							<img src="<?php echo get_template_directory_uri()?>/img/product-1-3.png" alt="">
						</div>
					</div>
				</div>
				<div class="trading__description">
					<h2 class="trading__title">Військова форма</h2>
					<p class="trading__txt">
						Форму шиють з матеріалів, які мають невелику вагу і високу стійкість до екстремальних навантажень. Як правило, такий одяг представлений ​​в камуфляжному забарвленні. Якщо ж говорити про колір камуфляжу, то він має різні природні тони і його підбирають в залежності від місцевості, де форму будуть носити.В загальному військова форма не сильно відрізняється від звичайного одягу. Проте для її виготовлення  використовують зносостійкі тканини і більш ретельно опрацьовують шви.
					</p>
					<p class="trading__addresses-txt">Адреси за якими ви можете придбати товар:</p>
					<a class="trading__addresses-btn" href="/obyekti-torgivli">Об’єкти торгівлі</a>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer() ;?>