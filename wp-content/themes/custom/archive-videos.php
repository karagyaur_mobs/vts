<?php
/**
 * Created by PhpStorm.
 * User: mobios starts
 * Date: 10.10.2018
 * Time: 17:33
 */
get_header(); ?>

<section class="container">
	<?php dimox_breadcrumbs() ?>
</section>

<section class="gallery">
	<div class="container">
		<h3 class="section__title"><?php echo get_the_title() ?></h3>
		<div class="gallery-page__inner gallery-page__inner_video">

			<?php

				if ( have_posts() ) : // если имеются записи в блоге.
				$paged = (get_query_var('term')) ? get_query_var('taxonomy') : 1;
					$current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;

					$args = array(
						'posts_per_page' => 9,
						'cat' => 11,
						'paged' => $current_page // текущая страница
					);

				query_posts($args);
				while (have_posts()) : the_post();  // запускаем цикл обхода материалов блога
				?>

				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 about__slide gallery gallery-page__video">
					<a class="fancybox fancybox_video mfp-iframe" data-fancybox="gallery" href="<?php the_field('video-link'); ?>">
						<img src="<?php echo the_post_thumbnail_url( 'full' ); ?>" alt="<?php echo the_title(); ?>" title="<?php echo the_title(); ?>" class="about__slide-img">
					</a>
					<h2 class="section-title fancybox_title-video" ><?php echo the_title(); ?></h2>
				</div>

			<?php
			endwhile;
				the_posts_pagination();
			endif;
			wp_reset_query();
			?>

		</div>
	</div>
</section>


<?php get_footer(); ?>
