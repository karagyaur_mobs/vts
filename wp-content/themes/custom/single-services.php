<?php
get_header() ;?>

    <section class="breadcrumbs-wrap container">
		<?php dimox_breadcrumbs() ?>
    </section>

    <section class="page-content__wrap">
        <div class="container">
			<?php while( have_posts() ) : the_post();
				$more = 1; // отображаем полностью всё содержимое поста
				the_content(); // выводим контент
			endwhile; ?>
        </div>
    </section>

    <section class="services">
        <div class="container">
            <h3 class="section__title"><?php single_post_title() ?></h3>
            <div class="services__inner">
	            <?php for ($i = 1; $i <= 10; $i++) { ?>
		            <?php if (get_field('services-title-' . $i)) { ?>
                        <div class="services__item questions__item">
                            <p class="services__question"><?php the_field('services-title-' . $i)?></p>
                            <div class="services__answer-wrap questions__hidden">
                                <p class="services__answer">
						            <?php the_field('services-address-' . $i)?>
                                </p>
                            </div>
                        </div>
		            <?php } ?>
	            <?php } ?>
            </div>
        </div>
    </section>

<?php get_footer() ;?>