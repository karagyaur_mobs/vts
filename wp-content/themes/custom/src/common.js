$(function() {


    // accordion
    $('.services__item').click(function () {
        if (!$(this).hasClass('active')) {
            $('.services__inner .services__item.active').removeClass('active').children('.questions__hidden').slideUp();
        }
        $(this).toggleClass('active').children('.questions__hidden').slideToggle();
    });



    $('.about__team-list-speciality_more').click(function () {
        var stuff = $(this).parents('.about__team-list-item');
        stuff.toggleClass('active');
        if (stuff.hasClass('active')) {
            $(this).text('Згорнути')
        } else {
            $(this).text('Детальніше')
        }
    // .text('Згорнути')
    });

    $('.certificates-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true,
        dots: false,
        infinite: false
    });

    $('.banner').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: true
        // responsive: [
        //     {
        //         breakpoint: 767,
        //         settings: {
        //             slidesToShow: 1,
        //             dots: true,
        //             autoplay: true
        //         }
        //     }
        // ]
    });

    $('.gallery__slider').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: true,
        dots: false,
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 3,
	                slidesToScroll: 1,
                    dots: true,
                    autoplay: false
                }
            },
	        {
		        breakpoint: 992,
		        settings: {
			        slidesToShow: 2,
			        dots: true,
			        autoplay: false
		        }
	        },
	        {
		        breakpoint: 768,
		        settings: {
			        slidesToShow: 1,
			        dots: true,
			        autoplay: false,
			        centerMode: true,
			        centerPadding: '0'
		        }
	        }
        ]
    });

    $(".news__list-inner").mCustomScrollbar({
        theme: "minimal-dark"
    });

    // if (screen.width > 767) {
    	// new WOW().init();
	// }


    $('.trading__poster1').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.trading__nav1'
    });
    $('.trading__nav1').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.trading__poster1',
        dots: false,
        arrows: true,
        centerMode: true,
        focusOnSelect: true,
	    responsive: [
		    {
			    breakpoint: 992,
			    settings: {
				    slidesToShow: 2,
				    slidesToScroll: 1
			    }
		    }
	    ]
    });

    $('.trading__poster2').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.trading__nav2',
    });
    $('.trading__nav2').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.trading__poster2',
        dots: false,
        arrows: true,
        centerMode: true,
        focusOnSelect: true,
	    responsive: [
		    {
			    breakpoint: 992,
			    settings: {
				    slidesToShow: 2,
				    slidesToScroll: 1
			    }
		    }
	    ]
    });



    // ===== Scroll to Top ====
    // $(window).scroll(function() {
    //     if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
    //         $('#return-to-top').fadeIn();    // Fade in the arrow
    //     } else {
    //         $('#return-to-top').fadeOut();   // Else fade out the arrow
    //     }
    // });
    // $('#return-to-top').click(function() {      // When arrow is clicked
    //     $('body,html').animate({
    //         scrollTop : 0                       // Scroll to top of body
    //     }, 500);
    // });

    $('.hamburger').click(function () {
        $(this).toggleClass('is-active').next('.hamburger-menu__wrap').slideToggle();
    });


    $("a[href='#pop']").magnificPopup({
        type: 'inline',
        focus: '#name',
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        midClick: true
    });

    $("a[href='#search']").magnificPopup({
        type: 'inline',
        focus: '#s',
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        midClick: true
    });


	//Contact form
	$('.callback').submit(function(){
		$.ajax({
			type: "POST",
			url: "/mail.php",
			data: $(this).serialize()
		}).done(function(){
			// $('.success').addClass('visible-success');
            alert("Спасибо за заявку! Мы с Вами свяжемся.");
			setTimeout(function(){
				$(this).trigger("reset");
				// $('.success').removeClass('visible-success');
			}, 3000);
		});
		return false;
	});

// Gallery VIDEO
	$('.mfp-iframe').each(function() { // the containers for all your galleries
		$(this).magnificPopup({
			delegate: 'a', // the selector for gallery item
			type: 'image',
			gallery: {
				enabled:true
			}
		});
	});



	//Phone-mask
	// $('.phone-us').mask('+0(000) 00-00-000');




});


// $(window).on('load', function(){
// 	setTimeout(function(){
// 		$('.preloader-wrapper').fadeOut();
// 	}, 500)
// });

