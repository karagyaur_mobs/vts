<?php
/**
 * Created by PhpStorm.
 * User: mobios starts
 * Date: 11.10.2018
 * Time: 12:09
 */
get_header() ;?>

	<section class="breadcrumbs-wrap container">
		<?php dimox_breadcrumbs() ?>
	</section>

	<section class="trading">
		<div class="container">
			<h3 class="section__title">Послуги</h3>
			<div class="trading__inner">
				<?php
				global $post;
				$args = array('posts_per_page' => -1, 'category' => 7);
				$myposts = get_posts($args);
				foreach ($myposts as $post) {
					setup_postdata($post); ?>
					<div class="trading__item">
						<div class="trading__examples">
							<div class="">
								<a class="more-overlay" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(415, 280)); ?></a>
							</div>
						</div>
						<div class="trading__description">
							<h2 class="trading__title">
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h2>
							<?php the_excerpt(); ?>
							<a class="trading__more" href="<?php the_permalink(); ?>">Детальніше</a>
						</div>
					</div>
					<?php
				}
				wp_reset_postdata(); ?>
			</div>
		</div>
	</section>

<?php get_footer() ;?>