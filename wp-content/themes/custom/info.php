<?php
/**
 * Created by PhpStorm.
 * User: mobios starts
 * Date: 12.10.2018
 * Time: 16:18
 *  Template Name: Публічна інформація
 */
get_header() ;?>

<section class="breadcrumbs-wrap container">
	<?php dimox_breadcrumbs() ?>
</section>

<section class="page-content__wrap">
	<div class="container">
		<h3 class="section__title"><?php the_title(); ?></h3>
		<?php while( have_posts() ) : the_post();
			$more = 1; // отображаем полностью всё содержимое поста
			the_content(); // выводим контент
		endwhile; ?>
	</div>
</section>

<section class="gallery">
	<div class="container">
		<div class="gallery__inner">
			<div class="gallery__slider">
				<?php $images = acf_photo_gallery('info-gallery', $post->ID); ?>
				<?php if( $images ):
					foreach($images as $image):
						$full_image_url = $image['full_image_url']; //Full size image url
						$image_url = acf_photo_gallery_resize_image($full_image_url, 350, 240); //Resized size to image url?>
						<div class="about__slide">
							<a class="fancybox" data-fancybox="gallery" href="<?php echo $full_image_url; ?>">
								<img src="<?php echo $image_url; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" class="about__slide-img">
							</a>
						</div>
					<?php endforeach;
				endif; ?>
			</div>
		</div>
	</div>
</section>

<section class="docs">
	<div class="container">
		<h3 class="section__title">Документи</h3>
		<div class="docs__inner row">
			<?php for ($i = 1; $i <= 10; $i++) { ?>
				<?php if (get_field('file-' . $i)) { ?>
					<div class="col-md-2">
						<?php $format = get_field('format-' . $i); ?>
						<a download class="docs__file docs__<?php echo $format ?>" href="<?php the_field('file-' . $i) ?>">
							<?php the_field('file-name-' . $i) ?>
						</a>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
</section>




<?php get_footer(); ?>
