<?php
/*
Template Name: Главная страница
*/
get_header() ;?>

<section class="banner">
    <div class="banner__item" style="background-image: url('<?php the_field('banner-image-1')?>')">
        <div class="container">
            <h1 class="banner__title">Концерн &laquo;Військторгсервіс&raquo;</h1>
            <h2 class="banner__subtitle">Державне господарське<br>об’єднання</h2>
        </div>
    </div>
    <div class="banner__item" style="background-image: url('<?php the_field('banner-image-1')?>')">
        <div class="container">
            <h1 class="banner__title">Концерн &laquo;Військторгсервіс&raquo;</h1>
            <h2 class="banner__subtitle">Державне господарське<br>об’єднання</h2>
        </div>
    </div>
    <div class="banner__item" style="background-image: url('<?php the_field('banner-image-1')?>')">
        <div class="container">
            <h1 class="banner__title">Концерн &laquo;Військторгсервіс&raquo;</h1>
            <h2 class="banner__subtitle">Державне господарське<br>об’єднання</h2>
        </div>
    </div>
</section>

<section class="about">
    <div class="container">
        <h3 class="section__title">Про &laquo;Військторгсервіс&raquo;</h3>
        <div class="about__inner row">
            <div class="about__item col-sm-4">
                <div class="about__num">
                    <span>1</span>
                </div>
                <p class="about__txt">
                    Концерн “Військторгсервіс” є державним господарським об’єднанням і належить до сфери управління Міністерства оборони України.
                </p>
            </div>
            <div class="about__item col-sm-4">
                <div class="about__num">
                    <span>2</span>
                </div>
                <p class="about__txt">
                    Діяльністю концерну: <br>
                    - забезпечення  ефективного функціонування та управління суб’єктами господарювання;<br>
                    - задоволення матеріально-побутових, культурних потреб та оздоровлення військовослужбовців та членів їх сімей;<br>
                    - контроль за ефективним та рентабельним використанням майна та фондів Міністерства оборони України.
                </p>
            </div>
            <div class="advantages__item col-sm-4">
                <div class="about__num">
                    <span>3</span>
                </div>
                <p class="about__txt">
                    Концерн “Військторгсервіс” об’єднав у своєму складі підприємства та установи сфери торгівлі, громадського харчування, лазнево-пральні комбінати, турбази та готелі.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="advantages">
    <div class="container">
        <h3 class="section__title">Переваги</h3>
        <div class="advantages__inner row">
            <div class="col">
                <img width="101" class="img-center" src="<?php echo get_template_directory_uri()?>/img/icons/advant-1.svg" alt="">
                <p class="advantages__txt">
                    Персонал, який у випадку необхідності може зразу перейти на умови особливого періоду
                </p>
            </div>
            <div class="col">
                <img width="52" class="img-center" src="<?php echo get_template_directory_uri()?>/img/icons/advant-2.svg" alt="">
                <p class="advantages__txt">
                    Гарантована висока якість і своєчасність надання послуг та виготовлення товару
                </p>
            </div>
            <div class="col">
                <img width="65" class="img-center" src="<?php echo get_template_directory_uri()?>/img/icons/advant-3.svg" alt="">
                <p class="advantages__txt">
                    Можливість здійснення контролю та впливу на якість продукції органом управління
                </p>
            </div>
            <div class="col">
                <img width="70" class="img-center" src="<?php echo get_template_directory_uri()?>/img/icons/advant-4.svg" alt="">
                <p class="advantages__txt">
                    Можливість здійснення харчування в польових умовах та в особливий період
                </p>
            </div>
            <div class="col">
                <img width="101" class="img-center" src="<?php echo get_template_directory_uri()?>/img/icons/advant-5.svg" alt="">
                <p class="advantages__txt">
                    Концерн, який охоплює усі регіони України
                </p>
            </div>
        </div>
    </div>
</section>

<section class="products">
    <div class="container">
        <h3 class="section__title">Наша продукція</h3>
        <div class="products__inner row">
            <div class="col-md-6 products__item">
                <a class="products__item-inner" href="/torgivlya">
                    <img class="img-center" src="<?php echo get_template_directory_uri() ?>/img/product-1.png" alt="">
                    <h3 class="products__title">Сувенірна продукція</h3>
                </a>
            </div>
            <div class="col-md-6 products__item">
                <a class="products__item-inner" href="/torgivlya">
                    <img class="img-center" src="<?php echo get_template_directory_uri() ?>/img/product-2.png" alt="">
                    <h3 class="products__title">Військова форма</h3>
                </a>
            </div>
        </div>
    </div>
</section>


<section class="gallery">
    <div class="container">
        <h3 class="section__title">Фотогалерея</h3>
        <div class="gallery__inner">
            <div class="gallery__slider">
		        <?php $images = acf_photo_gallery('gallery', $post->ID); ?>
		        <?php if( $images ):
			        foreach($images as $image):
				        $full_image_url = $image['full_image_url']; //Full size image url
				        $image_url = acf_photo_gallery_resize_image($full_image_url, 350, 240); //Resized size to image url?>
                        <div class="about__slide">
                            <a class="fancybox" data-fancybox="gallery" href="<?php echo $full_image_url; ?>">
                                <img src="<?php echo $image_url; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" class="about__slide-img">
                            </a>
                        </div>
			        <?php endforeach;
		        endif; ?>
            </div>
        </div>
    </div>
</section>

<section class="news">
    <div class="container">
        <h3 class="section__title">Новини</h3>
        <div class="news__inner row">
            <?php
	        global $post;
	        $args = array('posts_per_page' => 2, 'category' => 4);
	        $myposts = get_posts($args);
	        foreach ($myposts as $post) {
		        setup_postdata($post);
		        ?>
                <div class="news__item col-xs-12 col-lg-6 col-xl-4">
                    <a class="news__poster" href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(350, 235) ); ?></a>

                    <a class="news__head" href="<?php the_permalink(); ?>">
                        <?php echo mb_strimwidth(get_the_title(), 0, 55, '...');?>
                    </a>
                    <p class="news__descr"><?php trimmed_content(300 ); ?></p>
                    <div class="news__read-more-wrap">
                        <span class=""><?php echo get_the_date('d.m.Y')?></span>
                        <a class="news__read-more" href="<?php the_permalink(); ?>">Читати далi...</a>
                    </div>
                </div>
		    <?php
	        }
	        wp_reset_postdata();
	        ?>
            <div class="col-lg-12 col-xl-4">
	            <div class="news__list">
                    <div class="news__list-inner">
                        <?php
                        global $post;
                        $args = array('posts_per_page' => 20, 'category' => 4);
                        $myposts = get_posts($args);
                        $cat_link = get_category_link( 4 );
                        foreach ($myposts as $post) {
                            setup_postdata($post);
                            ?>
                            <a href="<?php the_permalink(); ?>" class="news__list-item">
                                <span class="news__list-date "><?php echo get_the_date('d.m' )?></span>
                                <p class="news__list-head"><?php the_title(); ?></p>
                            </a>
                            <?php
                        }
                        wp_reset_postdata();
                        ?>
                    </div>
                <a class="news__all" href="<?php echo $cat_link ?>">Дивитися всі новини</a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer() ;?>