<?php
/**
 * The template for displaying 404 pages (Not Found)
 */
get_header(); ?>

	<!--error section-->
	<section class="error-section">
		<div class="container">
            <p class="error-section__title">Помилка</p>
			<h1 class="error-section__num">404</h1>
            <p class="error-section__txt">
                Такої сторінки не існує:( <br>
                Але ви можете повернутися на  <a href="/">головну</a>
            </p>
		</div>
	</section>
	<!--error section END-->

<?php get_footer() ;?>