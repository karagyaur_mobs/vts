<?php
/**
 * Created by PhpStorm.
 * User: mobios starts
 * Date: 12.07.2018
 * Time: 14:10
 * Template Name: Керівництво
 */
get_header(); ?>

<section class="container">
	<?php dimox_breadcrumbs() ?>
</section>

<section class="about">
	<div class="container">
		<h1 class="section__title">Керівництво</h1>
        <div class="about__team">
            <div class="about__team-list-wrap">
	            <?php for ($i = 1; $i <= 10; $i++) { ?>
		            <?php if (get_field('staffer-name-' . $i)) { ?>
                        <div class="about__team-list-item">
                            <div class="about__team-list-img">
                                <img class="img-responsive" src="<?php the_field('staffer-photo-' . $i) ?>" alt="">
                            </div>
                            <div class="about__team-list-info">
                                <div class="about__team-list-info-name">
                                    <h5 class="about__team-list-name"><?php the_field('staffer-name-' . $i) ?></h5>
                                    <span class="about__team-list-speciality"><?php the_field('staffer-spec-' . $i) ?></span>
                                    <span class="about__team-list-speciality about__team-list-speciality_more">Детальніше</span>
                                </div>
                                <div class="about__team-list-info-docs">
                                    <p class="about__team-list-about">
							            <?php the_field('staffer-about-' . $i) ?>
                                    </p>
                                    <h6 class="about__team-list-cert">Документи</h6>
                                    <div class="certificates-slider">
							            <?php $images = acf_photo_gallery('staffer-cert-' . $i, $post->ID); ?>
							            <?php if( $images ):
								            foreach($images as $image):
									            $image_url = $image['full_image_url'];?>
                                                <div class="certificates-slide">
                                                    <a data-fancybox="gallery" href="<?php echo $image_url; ?>">
                                                        <img  src="<?php echo $image_url; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" class="img-responsive">
                                                    </a>
                                                </div>
								            <?php endforeach;
							            endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
		            <?php } ?>
	            <?php } ?>
            </div>
        </div>
        <div class="about__inner page-content__wrap">
			<?php while ( have_posts() ) : the_post();
				$more = 1; // отображаем полностью всё содержимое поста
				the_content(); // выводим контент
			endwhile; ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
