	<!--FOOTER-->
    <footer class="footer">
        <div class="container">
            <div class="footer__inner row">
                <div class="col-xs12 col-sm-6 footer__inner-margin col-md-3">
                    <a href="/">
                        <img src="<?php echo get_template_directory_uri()?>/img/footer-logo.png" alt="">
                    </a>
                    <div class="footer__socials">
                        <a href="#">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#AFD755;}
</style>
                                <g>
                                    <g>
                                        <g>
                                            <path class="st0" d="M287.8,167.7l0.1-44.2c0-23,1.6-35.3,34.6-35.3h61V0h-87.4c-84.8,0-104.3,43.8-104.3,115.8l0.1,52h-64.4V256
				h64.4v256h96l0.1-256h87.1l9.4-88.2h-96.7V167.7z"/>
                                        </g>
                                    </g>
                                </g>
</svg>
                        </a>
                        <a href="#">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#AFD755;}
</style>
                                <g id="XMLID_496_">
                                    <path id="XMLID_497_" class="st0" d="M5.3,144.6l69.1,25.8l26.8,86c1.7,5.5,8.5,7.5,12.9,3.9l38.5-31.4c4-3.3,9.8-3.5,14-0.4
		l69.5,50.5c4.8,3.5,11.6,0.9,12.8-4.9l50.9-244.9c1.3-6.3-4.9-11.6-10.9-9.3L5.2,129.4C-1.8,132.1-1.7,142,5.3,144.6z M96.9,156.7
		L232,73.5c2.4-1.5,4.9,1.8,2.8,3.7L123.3,180.9c-3.9,3.6-6.4,8.5-7.2,13.8l-3.8,28.1c-0.5,3.8-5.8,4.1-6.8,0.5L90.9,172
		C89.3,166.2,91.7,159.9,96.9,156.7z"/>
                                </g>
</svg>
                        </a>
                        <a href="#">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="-359 82 48 47.5" style="enable-background:new -359 82 48 47.5;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#A9CB5E;}
</style>
                                <path class="st0" d="M-334.5,95.5c2.9,0,5.5,1.2,7.4,3.1h12.9v-5.3h0c0-4.4-3.6-8-8-8h-21.9v10.1h-1.6V85.3h-0.9c-0.1,0-0.1,0-0.2,0
	v10.1h-1.6v-9.9c-0.4,0.1-0.8,0.2-1.2,0.4v9.5h-1.6v-8.6c-2,1.4-3.4,3.8-3.4,6.5v5.3h12.8C-340,96.7-337.4,95.5-334.5,95.5z
	 M-324.8,89.9c0-0.7,0.6-1.3,1.3-1.3h4.3c0.7,0,1.3,0.6,1.3,1.3v4.4c0,0.7-0.6,1.3-1.3,1.3h-4.3c-0.7,0-1.3-0.6-1.3-1.3V89.9z"/>
                                <path class="st0" d="M-329.2,99.8c-0.6-0.5-1.2-0.9-2-1.3c-1-0.5-2.1-0.7-3.3-0.7c-1.2,0-2.3,0.3-3.3,0.7c-0.7,0.3-1.4,0.8-2,1.3
	c-1.7,1.5-2.7,3.6-2.7,6c0,4.4,3.6,8,8,8c4.4,0,8-3.6,8-8C-326.5,103.5-327.6,101.3-329.2,99.8z M-334.5,111.7
	c-3.3,0-5.9-2.6-5.9-5.9c0-3.3,2.6-5.9,5.9-5.9c3.3,0,5.9,2.6,5.9,5.9C-328.6,109.1-331.2,111.7-334.5,111.7z"/>
                                <path class="st0" d="M-346.7,126h24.5c4.4,0,8-3.6,8-8V99.8h-11.8c1.2,1.7,1.9,3.8,1.9,6c0,5.7-4.6,10.4-10.4,10.4
	c-5.7,0-10.4-4.6-10.4-10.4c0-2.2,0.7-4.3,1.9-6h-11.8V118C-354.7,122.4-351.1,126-346.7,126z"/>
</svg>
                        </a>
                    </div>
                    <div class="footer__copyright">
                        <p class="footer__copyright-txt">&copy; 2018 Всі права захищені</p>
                        <p class="footer__copyright-txt">Сайт розроблено <a href="//mobios.ua" rel="nofollow" target="_blank">Mobios</a></p>
                    </div>
                </div>
                <div class="col-xs12 col-sm-6 footer__inner-margin col-md-3">
	                <?php wp_nav_menu(array(
		                'menu' => 'footer-1',
		                'menu_class' => 'footer__menu'
	                )); ?>
                </div>
                <div class="col-xs12 col-sm-6 footer__inner-margin col-md-3 offset-lg-1">
	                <?php wp_nav_menu(array(
		                'menu' => 'footer-2',
		                'menu_class' => 'footer__menu'
	                )); ?>
                </div>
                <div class="col-xs12 col-sm-6 footer__inner-margin col-md-3 col-lg-2">
                    <h3 class="footer__title">Контакти</h3>
                    <a class="footer__link" href="tel:+38 (063) 432-34-56">+ 38 (063) 432-34-56</a>
                    <a class="footer__link" href="tel:+38 (063) 432-34-56">+ 38 (063) 432-34-56</a>
                    <a class="footer__link" href="mailto:info@gmail.com">info@gmail.com</a>
                </div>
            </div>
        </div>
	</footer>
	<!--FOOTER-END-->


    <div class="white-popup mfp-hide zoom-anim-dialog" id="pop">
        <p class="section__title section__title_small">Заполните Вашу контактную информацию</p>
        <form class="form__body callback">
            <input type="hidden" name="project_name" value="Заявка">
            <input type="hidden" name="admin_email" value="mobios.dev@gmail.com">
            <input type="hidden" name="form_subject" value="Новая заявка с сайта">

            <input class="form__input" name="Имя" required id="name" type="text" placeholder="Ваше имя *">
            <input class="form__input" name="Почта" required type="email" placeholder="Email *">
            <input class="form__input" name="Телефон" required type="tel" placeholder="Ваш номер телефона *">

            <div class="form__btn-wrap">
                <button class="form__btn banner__btn banner__btn_small" type="submit">Записаться</button>
            </div>
        </form>
    </div>

    <div class="white-popup mfp-hide zoom-anim-dialog" id="search">
        <p class="section__title section__title_small">Пошук</p>
	    <?php get_search_form(); ?>
    </div>

	<div class="white-popup mfp-hide zoom-anim-dialog" id="js-mfpVideoe">
		<p class="section__title section__title_small">ВІДЕОРОЛИК ПРО КОНЦЕРН “ВІЙСЬКТОРГСЕРВІС”</p>
			<div class="pop-video">VIDEO</div>
	</div>

<!--    <div id="return-to-top"></div>-->

	<?php wp_footer(); ?> 
</body>
</html>