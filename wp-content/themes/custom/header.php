<!DOCTYPE html>
<html lang="ru-RU">

<head>

	<meta charset="utf-8">

	<title><?php echo wp_get_document_title() ?></title>

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/favicon/manifest.json">
	<!-- Chrome, Firefox OS and Opera -->
	<meta name="theme-color" content="#00cae3">
	<!-- Windows Phone -->
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="msapplication-navbutton-color" content="#00cae3">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#00cae3">


	<?php wp_head() ;?>
</head>

<body <?php body_class(); ?>>

<!--	<div class="preloader-wrapper">-->
<!--		<img src="--><?php //echo get_template_directory_uri(); ?><!--/img/preloader.svg" alt="прелоадер" class="preloader">-->
<!--	</div>-->

	<!--HEADER-->
<header class="header">
    <div class="container">
        <div class="header__inner">
            <div class="header__logo-wrap">
                <a href="/">
                    <img src="<?php echo get_template_directory_uri()?>/img/logo.png" alt="">
                </a>
            </div>
            <nav class="header__nav">
                <div class="burger-menu__wrap">
                    <div class="hamburger hamburger--slider">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </div>
                    <div class="hamburger-menu__wrap">
				        <?php wp_nav_menu(array(
					        'menu' => 'burger',
					        'menu_class' => 'burger-menu'
				        )); ?>
				        <?php wp_nav_menu(array(
					        'menu' => 'main',
					        'menu_class' => 'mobile-main-menu'
				        )); ?>
                    </div>
                </div>
		        <?php wp_nav_menu(array(
			        'menu' => 'main',
			        'menu_class' => 'main-menu'
		        )); ?>
            </nav>
            <div class="header__search">
                <a href="#search">
                    <img width="21" src="<?php echo get_template_directory_uri()?>/img/icons/search.svg" alt="">
                </a>
            </div>
            <div class="header__phone">
                <a class="header__phone-link" href="tel:+38 (063) 432-34-56">+38 (063) 432-34-56</a>
            </div>
        </div>
    </div>
</header>
	<!--HEADER-END-->