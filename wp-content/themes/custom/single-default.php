<?php
get_header() ;?>

    <section class="breadcrumbs-wrap container">
		<?php dimox_breadcrumbs() ?>
    </section>

    <section class="page-content__wrap">
        <div class="container">
            <h3 class="section__title"><?php the_title(); ?></h3>
            <?php the_post_thumbnail(array(540, 360)) ?>
            <span class="page-content__date"><?php echo get_the_date('d.m.Y')?></span>
            <?php while( have_posts() ) : the_post();
				$more = 1; // отображаем полностью всё содержимое поста
				the_content(); // выводим контент
			endwhile; ?>
        </div>
    </section>

<?php get_footer() ;?>